"use strict";
exports.__esModule = true;
var Conta = /** @class */ (function () {
    function Conta() {
    }
    Conta.prototype.saque = function (valor) {
        if (this.saldo >= valor) {
            this.saldo -= valor;
            return valor;
        }
        else {
            return 0;
        }
    };
    Conta.prototype.deposito = function (valor) {
        if (valor >= 0) {
            this.saldo += valor;
        }
    };
    Conta.prototype.getSaldo = function () {
        return this.saldo;
    };
    Conta.prototype.setAgencia = function (agencia) {
        this.agencia = agencia;
    };
    Conta.prototype.setNumero = function (numero) {
        this.numero = numero;
    };
    return Conta;
}());
exports.Conta = Conta;
var conta1 = new Conta();
var conta2 = new Conta();
conta1.deposito(1000);
conta1.saque(50);
conta1.getSaldo();
conta1.setAgencia(9371);
conta1.setNumero(229239);
