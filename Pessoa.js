"use strict";
exports.__esModule = true;
var primeiraAula_1 = require("./primeiraAula");
var Pessoa = /** @class */ (function () {
    function Pessoa(nome, cpf, conta) {
        this.nome = nome;
        this.cpf = cpf;
        this.conta = conta;
    }
    return Pessoa;
}());
exports.Pessoa = Pessoa;
var conta1 = new primeiraAula_1.Conta();
var Bernardo = new Pessoa("Bernardo Mayrinck", "18013497704", conta1);
