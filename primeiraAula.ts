export class Conta{
    private numero: number;
    private agencia: number;
    private saldo: number;

    public saque(valor:number): number {
        if (this.saldo >= valor){
            this.saldo -= valor;
            return valor;
        }else{
            return 0;
        }
    }
    public deposito(valor:number): void{
        if(valor>=0){
            this.saldo += valor;
        }
    }
    public getSaldo(): number{
        return this.saldo
    }
    public setAgencia(agencia: number){
        this.agencia = agencia
    }
    public setNumero(numero: number){
        this.numero = numero
    }
}
let conta1: Conta = new Conta();
let conta2 = new Conta();


conta1.deposito(1000);
conta1.saque(50);

conta1.getSaldo();
conta1.setAgencia(9371);
conta1.setNumero(229239)