import { Conta } from "./primeiraAula";

export class Pessoa{
    private nome: string;
    private cpf: string;
    private conta: Conta;
    private static qtdPessoas:number=0;

    constructor( nome:string, cpf:string, conta: Conta, public rg: string){
        this.nome = nome;
        this.cpf = cpf;
        this.conta = conta;
        this.rg = rg

        Pessoa.qtdPessoas +=1;
    }
}

let conta1 = new Conta();

let Bernardo = new Pessoa("Bernardo Mayrinck","18013497704",conta1,"30.640.651-3");
